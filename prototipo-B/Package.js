const puppeteer = require('puppeteer');

const takeScreenshot = async () => {
  const browser = await puppeteer.launch({ headless: false});
  const page = await browser.newPage();
  const options = {
    path: "image/ejemploX.png",
    omitBackground: true,
    fullPage: true
  };
  await page.goto("https://uah.es/es/");
  await page.screenshot(options);


    console.log('its showing');

  await browser.close();
};

takeScreenshot();
